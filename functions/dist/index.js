const express = require('express');
var admin = require('firebase-admin');
const BodyParser = require('body-parser');
const app = express();
admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: 'https://delta-button-276212.firebaseio.com'
});
let db = admin.firestore();
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
app.get('/', async (req, res, next) => {
    try {
        console.log('getting notes');
        const noteSnapshot = await db.collection('notes').get();
        const notes = [];
        noteSnapshot.forEach((doc) => {
            notes.push({
                id: doc.id,
                data: doc.data()
            });
        });
        res.json(notes);
    }
    catch (e) {
        next(e);
    }
});
app.get('/:id', async (req, res, next) => {
    try {
        const id = req.params.id;
        if (!id)
            throw new Error('id is blank');
        const note = await db.collection('notes').doc(id).get();
        if (!note.exists) {
            throw new Error('note does not exists');
        }
        res.json({
            id: note.id,
            data: note.data()
        });
    }
    catch (e) {
        next(e);
    }
});
app.post('/', async (req, res, next) => {
    try {
        const text = req.body.text;
        if (!text)
            throw new Error('Text is blank');
        const data = { text };
        const ref = await db.collection('notes').add(data);
        res.json({
            id: ref.id,
            data
        });
    }
    catch (e) {
        next(e);
    }
});
app.put('/:id', async (req, res, next) => {
    try {
        const id = req.params.id;
        const text = req.body.text;
        if (!id)
            throw new Error('id is blank');
        if (!text)
            throw new Error('Text is blank');
        const data = { text };
        const ref = await db.collection('notes').doc(id).set(data, { merge: true });
        res.json({
            id,
            data
        });
    }
    catch (e) {
        next(e);
    }
});
app.delete('/:id', async (req, res, next) => {
    try {
        const id = req.params.id;
        if (!id)
            throw new Error('id is blank');
        await db.collection('notes').doc(id).delete();
        res.json({
            id
        });
    }
    catch (e) {
        next(e);
    }
});
const PORT = process.env.PORT || 3000; // local and docker
app.listen(PORT, () => console.log(`test Firebase app listening on port ${PORT}!`));
//# sourceMappingURL=index.js.map